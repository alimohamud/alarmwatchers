//
//  NotificationViewController.swift
//  AlarmClock
//
//  Created by Muaj Yang on 4/14/21.
//

import Foundation
import SwiftUI

//Creating a function where it tells the user to go into sleep mode so the application can run.
// This function serves as a notification 
